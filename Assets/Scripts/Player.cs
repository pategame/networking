﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player : NetworkBehaviour {

    Rigidbody2D body;
	// Use this for initialization
	void Start ()
    {
        body = GetComponent<Rigidbody2D>();
	
	}
 
    void Update ()
    {
        if (isServer)
        {


            if (Input.GetAxis("Horizontal") != 0)
            {
                body.transform.position += new Vector3(Input.GetAxis("Horizontal") * 10f * Time.deltaTime, 0);
            }
        }

    }
    void OnCollisionEnter2D(Collision2D coll)
    {
      if (coll.gameObject.tag == "BallPoint")
        {
            GuiInterface.instance.AddPoints();
            Destroy(coll.gameObject);
        }
    }
}
