﻿using UnityEngine;
using System.Collections;

public class Map_Generator : MonoBehaviour {

    public GameObject block;
    public Camera cam;


	// Use this for initialization
	void Start ()
    {
        Generator();
    }
	
	float maxY()
    {
        float temp;
        temp = cam.ScreenToWorldPoint(cam.pixelRect.max).y;
        return (temp);
    }
    float minX()
    {
        float temp;
        temp = cam.ScreenToWorldPoint(cam.pixelRect.min).x;
        return (temp);
    }

    void Generator()
    {
        int count = 0;
        int columns = 14;
        int rows = Random.Range(7,13);
        print(columns + " " + rows);
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                int rnd = Random.Range(0, 101);
                if (rnd % 4 != 0)
                {
                    string name = "Block " + j.ToString() + " " + i.ToString();
                    CreateBloc(new Vector2((minX() + 1.05f) + i * 1.2f, (maxY() - 0.5f) - j * 0.5f),name);
                    count += 1;
                }
            }
            GuiInterface.instance.count = count;
            GuiInterface.instance.ShowCount();
           
        }
    }
    void CreateBloc(Vector2 Pos, string _name)
    {
        GameObject g = Instantiate(block);
        g.transform.SetParent(transform);
        g.transform.position = Pos;
        g.name = _name;
    }

}
