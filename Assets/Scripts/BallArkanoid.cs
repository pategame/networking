﻿using UnityEngine;
using System.Collections;

public class BallArkanoid : MonoBehaviour {

    public Rigidbody2D body;
	// Use this for initialization
	void Start ()
    {
        body = GetComponent<Rigidbody2D>();
        
	}

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && body.isKinematic == true)
        {
            transform.parent = null;
            body.isKinematic = false;
            body.AddForce(new Vector2(50, 300));
        }
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Block")
        {
            coll.gameObject.GetComponent<Block>().MinusDefense(1);
        }
      

    }
    void OnBecameInvisible()
    {
        GuiInterface.instance.FAIL();
    }
}
