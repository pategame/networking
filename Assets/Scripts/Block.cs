﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

    public int defense;

    // Use this for initialization
    void Start()
    {
        defense = Random.Range(0, 4);
        Recolor();
     


    }
    void Recolor()
    {
        Color newcolor = Color.white;
        switch (defense)
        {
            case (1):
                newcolor = Color.green;
                break;
            case (2):
                newcolor =  Color.blue;
                break;
            case (3):
                newcolor = Color.magenta;
                break;

        }
        GetComponent<SpriteRenderer>().color = newcolor;
    }
    public void MinusDefense(int value)
    {
        defense -= value;
        Recolor();

        if (defense < 0)
        {
            GuiInterface.instance.MinusBrick();
            Destroy(gameObject);
        }
        
        
    }
	
	
}
