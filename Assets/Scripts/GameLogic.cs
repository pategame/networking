﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GameLogic : NetworkBehaviour {

    public GameObject Ball;
    Camera cam;
	// Use this for initialization
	void Start ()
    {
        Time.timeScale = 1;
        cam = GameObject.FindObjectOfType<Camera>();
        StartCoroutine(Generate());
	}
    float maxY()
    {
        float temp;
        temp = cam.ScreenToWorldPoint(cam.pixelRect.max).y;
        return (temp);
    }
    float minX()
    {
        float temp;
        temp = cam.ScreenToWorldPoint(cam.pixelRect.min).x;
        return (temp);
    }
    IEnumerator Generate()
    {
        while (Time.timeScale == 1)
        {
            SpawnBall();
            yield return new WaitForSeconds(1.5f);
        }
    }
    [Server]
    void SpawnBall()
    {
            var b = Instantiate(Ball);
            b.transform.position = new Vector3(Random.Range(minX() + 0.5f, -minX() - 0.5f), maxY() + 1, 0);
            NetworkServer.Spawn(b);
    }
}
