﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Net;
using System.Collections;

public class Menu : NetworkBehaviour {

    NetworkManager manager;
    public GameObject connectorpanel;
    public Text adress;
    string adressforserv;

	// Use this for initialization
	void Start ()
    {
        string host = Dns.GetHostName();
        IPAddress ip = Dns.GetHostByName(host).AddressList[1];
        adressforserv = ip.ToString();
   
    
    manager = GameObject.FindObjectOfType<NetworkManager>();
       // StartCoroutine(CheckIP());


	}
	public void OpenClosePanel(bool onoff)
    {
        connectorpanel.SetActive(onoff);
    }
    public void Connect()
    {
        manager.networkAddress = adress.text.ToString();
        manager.serverBindAddress = adress.text.ToString();
        manager.StartClient();
    }
	public void StartGame()
    {
        
    
        manager.serverBindAddress = adressforserv.ToString();
        manager.networkAddress = adressforserv.ToString();
        NetworkServer.Reset();
        manager.StopHost();
        manager.StartServer();
      ///  manager.StartHost();


    }
    public void Exit()
    {
        Application.Quit();
    }

    IEnumerator CheckIP()
    {
        WWW myExtIPWWW = new WWW("http://checkip.dyndns.org");
        if (myExtIPWWW == null) yield return new WaitForSeconds(0.1f);
        yield return myExtIPWWW;
        var myExtIP = myExtIPWWW.data;
        myExtIP = myExtIP.Substring(myExtIP.IndexOf(":") + 1);
        myExtIP = myExtIP.Substring(0, myExtIP.IndexOf("<"));
        adressforserv = myExtIP.Remove(0,1);
        print(adressforserv);
    }

}
