﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;

public class GuiInterface : NetworkBehaviour {

    public GameObject pause, finish;
    public Text text_on_finish, text_in_game,ip;
    [SyncVar]
    public int count;
    public bool arkanoid;
   
    void Start()
    {
        ShowCount();
        ip.text = GameObject.FindObjectOfType<NetworkManager>().serverBindAddress.ToString();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Pause();


    }

    [Server]
    public void Restart()
    {
        GameObject.FindObjectOfType<NetworkManager>().StartHost();
    }
   public void Pause()
    {
        if (pause.activeSelf == false)
        {
            Time.timeScale = 0;
            pause.SetActive(true);
            return;
        }
        else
        {
            Time.timeScale = 1;
            pause.SetActive(false);
            return;
        }

    }
 
    public void GoToMenu()
    {
        NetworkServer.Shutdown();
        Application.LoadLevel(0);
    }
    public void FAIL()
    {
        Time.timeScale = 0;
        finish.SetActive(true);
        text_on_finish.text = "You Fail!";
        NetworkServer.Shutdown();
       
    }
    public void Win()
    {
        Time.timeScale = 0;
        finish.SetActive(true);
        text_on_finish.text = "You Win!";
    }
    public void ShowCount()
    {
        if (arkanoid == true)
        text_in_game.text = "Lost: " + count.ToString();
        else text_in_game.text = "Score: " + count.ToString();
    }
    public void AddPoints()
    {
        count += 1;
        ShowCount();
        
    }

    public void MinusBrick()
    {
        count -= 1;
        ShowCount();
        if (count == 0) Win();
    }
    static GuiInterface _instance;
    public static GuiInterface instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GuiInterface>();

            }
            return _instance;
        }
    }
    void OnApplicationQuit()
    {
        NetworkServer.Shutdown();
    }
}
